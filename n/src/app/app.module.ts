import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { ServicesComponent } from './services/services.component';
import { EmergencyComponent } from './emergency/emergency.component';
import { ChooseComponent } from './choose/choose.component';
import { FooterComponent } from './footer/footer.component';
import { HousekeepingComponent } from './My-services/housekeeping/housekeeping.component';
import { PestControlComponent } from './My-services/pest-control/pest-control.component';
import { SecurityComponent } from './My-services/security/security.component';
import { StaffingComponent } from './My-services/staffing/staffing.component';
import { ProjectmanagementComponent } from './My-services/projectmanagement/projectmanagement.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    BannerComponent,
    ServicesComponent,
    EmergencyComponent,
    ChooseComponent,
    FooterComponent,
    HousekeepingComponent,
    PestControlComponent,
    SecurityComponent,
    StaffingComponent,
    ProjectmanagementComponent,
    AboutComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
