import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { HousekeepingComponent } from './My-services/housekeeping/housekeeping.component';
import { PestControlComponent } from './My-services/pest-control/pest-control.component';
import { ProjectmanagementComponent } from './My-services/projectmanagement/projectmanagement.component';
import { SecurityComponent } from './My-services/security/security.component';
import { StaffingComponent } from './My-services/staffing/staffing.component';
import { ServicesComponent } from './services/services.component';

const routes: Routes = [
  {path: "", component: HomeComponent},

  {path: "Home", component: HomeComponent},

  {path: "About", component: AboutComponent},

  {path: "Contact", component: ContactComponent},

  {path: "Housekeeping", component: HousekeepingComponent},

  {path: "Pest-Control", component: PestControlComponent},

  {path: "Project-Managemeent", component: ProjectmanagementComponent},

  {path: "Security", component: SecurityComponent},

  {path: "Staffing", component: StaffingComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
